#!/bin/bash

WASM_MODULE_URL="https://gitlab.com/api/v4/projects/38718633/packages/generic/golang/0.0.5/simple.wasm"
GITLAB_PAT=""
WASM_FILE="./src/simple-go-plugin/simple.wasm"

curl \
  --header "PRIVATE-TOKEN: ${GITLAB_PAT}" \
  --upload-file ${WASM_FILE} \
  "${WASM_MODULE_URL}"
    