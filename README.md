# golang-wasm-front

## Setup

### Docker image

- Change the value of `IMAGE_REGISTRY` into `/tools/extism-image/.env`
- Run `./build.sh`
- Run `./push.to.gitlab.registry.sh` (with the appropriate credentials: GitLab handle + Personal Access token)

### GitLab Personal AccessToken

- Set `GITLAB_PAT` into the CI/CD variables
  - used to publish the release to the package registry

### Pipeline

> Update this part into `.gitlab-ci.yml`
```yaml
variables:
  CI_PARENT_GROUP: k33g-demos
```


## Test (run) code locally

```bash
TODO
```

## How to get `index.esm.js`

```bash
cd src/web
npm install @extism/runtime-browser
cp ./node_modules/@extism/runtime-browser/dist/index.esm.js extism.js

rm -rf ./node_modules
rm package-lock.json
rm package.json
```


## Useful links

- https://gitlab.com/make-ci-lovable/experiments
- https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues
- https://editorconfig.org/
- https://blog.suborbital.dev/foundations-wasm-in-golang-is-fantastic
- https://github.com/danieljoos/go-wasm-examples/blob/master/wasm4/main.go
- https://gitlab.com/wasmcooking/snowcamp-2022

### Value Streams Analytics

With scoped labels:
- https://docs.gitlab.com/ee/user/group/value_stream_analytics/#label-based-stages-for-custom-value-streams
