#!/bin/bash


export WASM_FILE_NAME=simple.wasm
export WASM_SRC_PATH=src/simple-go-plugin
export WASM_FILE_PATH="${WASM_SRC_PATH}/${WASM_FILE_NAME}"

export WASM_MODULE="${WASM_FILE_NAME}"
export WASM_PACKAGE="golang"
export WASM_VERSION="X.X.X"

curl --header "PRIVATE-TOKEN: ${GITLAB_PAT}" --upload-file ${WASM_FILE_PATH} "${WASM_MODULE_URL}"
