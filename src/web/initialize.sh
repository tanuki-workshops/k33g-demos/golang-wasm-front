#!/bin/bash
npm install @extism/runtime-browser
cp ./node_modules/@extism/runtime-browser/dist/index.esm.js extism.js

rm -rf ./node_modules
rm package-lock.json
rm package.json

