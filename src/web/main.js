import { ExtismContext } from "./extism.js"

const manifest = { wasm: [{ path: "simple.wasm" }] }

const ctx = new ExtismContext()
const plugin = await ctx.newPlugin(manifest)

let output = await plugin.call("say_hello", "Hey 👋 I'm Bob! 😀")
let jsonOutput = JSON.parse(new TextDecoder().decode(output))

console.log(jsonOutput)

document.querySelector("h1").innerText = jsonOutput.input
document.querySelector("h2").innerText = jsonOutput.message
